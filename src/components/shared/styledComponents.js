/* eslint-disable */
import styled from 'vue-styled-components';
export const LineSeparator = styled.hr`
  margin: 0 auto;
  margin-bottom: 2rem;
  width: 18rem;
  height: 6px;
  background: url(http://ibrahimjabbari.com/english/images/hr-11.png) repeat-x 0 0;
  border: 0;
`;
